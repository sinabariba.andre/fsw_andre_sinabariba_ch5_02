const http = require("http");
const fs = require("fs");


const onRequest= (request, response) =>{
// console.log(request.url, request.method);
    let path = './views/';

    switch(request.url){
        case '/':
        path+= 'index.html'
        response.statusCode = 200;
        break;
        case '/about':
        path+= 'about.html'
        response.statusCode = 301;
        break;
        case '/contact':
        path = path + 'contact.html'
        break;
    default:
        path = path + '404.html'
        response.statusCode = 404;
    }

    //set header content type
    response.setHeader("Content-Type", "text/html")
    

    // Send an html file
    fs.readFile(path, null, (err,data) =>{
        if (err){
            response.writeHead(404)
            response.write("file not found");
        }else{
            response.write(data)
        }
        response.end();
    })
}


http.createServer(onRequest).listen(8000,'localhost', ()=>{console.log('Listen for request')});
